package aplicacion.android.luisdominguez.gendra.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import aplicacion.android.luisdominguez.gendra.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends android.app.Fragment implements OnMapReadyCallback{

    private View vistaRaiz;
    private MapView vistaMapa;
    private GoogleMap gmap;



    public MapaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vistaRaiz = inflater.inflate(R.layout.fragment_mapa, container, false);
        return vistaRaiz;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        vistaMapa = (MapView) vistaRaiz.findViewById(R.id.map);
        if(vistaMapa != null){
            vistaMapa.onCreate(null);
            vistaMapa.onResume();
            vistaMapa.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        gmap = googleMap;

        LatLng lugar = new LatLng(37.40911491941731, -5.99075691250005);//Latitud y longitud
        gmap.addMarker(new MarkerOptions().position(lugar).title("Hola desde Sevilla"));

        CameraPosition camara = new CameraPosition.Builder()
                .target(lugar)
                .zoom(15)
                .bearing(90)
                .tilt(30)
                .build();

        gmap.animateCamera(CameraUpdateFactory.newCameraPosition(camara));
    }
}
