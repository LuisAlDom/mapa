package aplicacion.android.luisdominguez.gendra.activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import aplicacion.android.luisdominguez.gendra.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);//Se captura el mapFrament
        mapFragment.getMapAsync(this);//Obtiene el mapa
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //mMap.setMinZoomPreference(10);//Predetermina el zoom Minimo
        //mMap.setMaxZoomPreference(15);//Predetermina el zoom Maximo

        // Add a marker in Sydney and move the camera
        LatLng sevilla = new LatLng(37.40911491941731, -5.99075691250005);//Latitud y longitud
        mMap.addMarker(new MarkerOptions().position(sevilla).title("Hola desde sevilla").draggable(true));//Lugar a donde movera la camara

        CameraPosition camara = new CameraPosition.Builder()
                .target(sevilla)
                .zoom(15)//tipo de zoom verificar en la documentacion
                .bearing(90)//Orientacion de la camara para el este
                .tilt(30)//Grados de inclinacion en la muestra de mapa
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camara));//Camara animada
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sevilla));//Es donde se posicionara la camara al iniciar el programa


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "Click en: \n" +
                        "Lat: " + latLng.latitude + "\n" +
                        "Long: " + latLng.longitude, Toast.LENGTH_SHORT).show();
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "Click Largo en: \n" +
                        "Lat: " + latLng.latitude + "\n" +
                        "Long: " + latLng.longitude, Toast.LENGTH_SHORT).show();
            }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {//Cuando se empieza a arrastrar

            }

            @Override
            public void onMarkerDrag(Marker marker) {//Mientras se esta arrastrando

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {//Cuando Termina de arrastrar
                Toast.makeText(MapsActivity.this, "Marcador arrastrado a: \n" +
                        "Lat: " + marker.getPosition().latitude + "\n" +
                        "Long: " + marker.getPosition().longitude, Toast.LENGTH_SHORT).show();

                Toast.makeText(MapsActivity.this, "" + marker.getPosition().longitude, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
