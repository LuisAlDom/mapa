package aplicacion.android.luisdominguez.gendra.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import aplicacion.android.luisdominguez.gendra.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BienvenidoFragment extends android.app.Fragment {


    public BienvenidoFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bienvenido, container, false);
    }

}
