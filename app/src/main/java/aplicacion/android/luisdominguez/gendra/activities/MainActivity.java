package aplicacion.android.luisdominguez.gendra.activities;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.maps.MapFragment;

import aplicacion.android.luisdominguez.gendra.R;
import aplicacion.android.luisdominguez.gendra.fragments.BienvenidoFragment;
import aplicacion.android.luisdominguez.gendra.fragments.MapaFragment;

public class MainActivity extends AppCompatActivity {

    Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentFragment = new BienvenidoFragment();
        changeFragment(currentFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_bienvenido:
                currentFragment = new BienvenidoFragment();
                break;
            case R.id.menu_mapa:
                currentFragment = new MapaFragment();
                break;
        }
        changeFragment(currentFragment);
        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment fragment){
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();
    }
}
